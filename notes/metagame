Overgame:
(writing under the influence of Castle Doctrine (although I'd been planning
something like this since long before I heard of CD!))

The players are the members of the Guild of Locksmiths. A venerable tradition
in this guild has the honour of a member determined by the number of silver
hook picks they possess. Tradition further has it that members must store
their collection of picks in coffers in the main hall of the guild, locked
behind fiendish yet pickable locks of their own devising. Members may freely
attempt to pick their fellows' locks, using their silver hooks, and take the
picks found within. New members start with 5(?) hooks. If a hook gets stuck
during an attempt on a coffer, it goes in that coffer.

Problems: there needs to be some cost to producing your own locks, or optimal
play would have you put each hook in a separate well-locked coffer, leading
to tiresomeness. And having every lock be maximally complicated would be a bit
boring - there should be costs associated to making complex locks. Also,
there's every incentive to not bother making a proper lock until you've made
some winnings, and to keep starting new characters until you have.

So forget the silver hooks - replace with a generic guild-internal currency,
which can be spent on picks and on coffers and lock components.

To deal with the latter problem: could make it such that lock designs are
one-offs - you can't edit it once it's complete, you can't pick other
locks until you have designed a lock to store any gains behind, and you can't
design further locks until your existing locks have withstood a few pick
attempts. This would also do something to minimize the cd-style "just one more
little edit" addictiveness.

To prevent cheating, we should give the client every possible convenience
feature. If there's no hidden information, then that includes undo. With undo, 
we need a time-limit for there to be any challenge.

Considering hidden information:
    How to ensure that locks are nonetheless fun, in the way cd's often
    aren't?

    It would be thematic to have vision based on sound and touch: so every time a
    part of the lock moves, it is shown, and everything in the vicinity of your
    tools is shown. 

    But however we do it, hidden information of a kind which prevents
    undo-cheating will surely end up allowing traps avoidable only through
    lucky guessing. Hrm.

So instead aim to have the mechanics sufficiently complicated that locks can
be made which are not susceptible to brute-force attacks, even ones guided by
an undo-wielding, keysmashing, unthoughtful human. With 15 possible moves each
turn, that perhaps isn't so implausible.

We can also make the time limit more severe by having a turn limit as well as
a time limit, and then using a fischer-clock system for the time limit, such
that you have to commit to some moves reasonably quickly. That would make undo
much less useful (although it should still be incorporated into the official
client, even if not well advertised). Turn and hence time limit would be, say,
triple the number of turns it took the creator when sey demonstrated
solvability.

Foiling sock-puppet attempts to get around the "one attempt per lock per
player" rule: don't give the players free choice over which locks to attempt.
Either only give them access to some random subset of the available locks, or
perhaps don't let them pick locks at all - instead they just indicate the
general hardness they want to go for.

Problem: if the complexity of a lock is bounded by cost, we should expect
cheap locks, however ingenious, to be consistently beatable by experienced
players. Solution: make sure this isn't true! Also: make the wager you stand
to lose if you fail to pick a lock be based on your own wealth?

After further thought, I am favouring equality in locks. You pay for the
coffer; locks are free. But locks are severely limited in size (say,
sufficiently that they can be displayed on an 80x24 terminal...), so it will
take some cunning to make a difficult lock.

You can design locks whenever you want, and maintain a library of designed
locks linked to your account. You start with a coffer, and new ones cost some
certain number of picks (10?). You can store as many picks as you like in a
coffer, but you might be wise to spread the risk.

It is intended that each human has just one account. To ensure this, new
players are required to undergo initiation before they can start picking
others' locks.  This is essentially a tutorial, getting you to pick some
predesigned locks. Furthermore, fresh initiates may not try high-level player
locks.

If a player runs out of silver picks: if sey has spare coffers, one is sold
back to the guild; else, sey is gifted valueless iron picks to try seir luck
with, say 5 per day (doled out all at once). You must use silver picks if you
have any. You start the game with 5 iron and 5 silver.

There are also valueless practice locks you can test your skills on without
wagering a pick - these should include a "hall of fame" of the locks which
withstood the most pick attempts before being picked.


Alternatively: can we judo sock puppets? Design the metagame such that there's
no advantage to having multiple accounts?


Misc:

http://www.haskellforall.com/2012/07/first-class-modules-without-defaults.html 



Maybe drop the chess clock in favour of a simple time limit depending on lock
complexity, with full undo? Easier to implement (could use http, or even email
(although delays would be a problem, as would forged timestamps)), and less
stressful. Would mean that simple traps lose importance - not sure if that's
good or bad.

Going this route, we could even copy CD's technique of checking that the
submitted solution really is one to be run as a batch job - meaning that the
server can run as a cgi script on sdf, with the checking being done as a
cronjob on legonz.


Architectural musings:
The client exists to
    (i) design and test lockfiles
    (ii) solve locks
    (iii) view solutions
so inputs and outputs are lockfiles and command-sequences.

Remark: given undo, there's not much point in uploading failed attempts -
only the whole undo tree would really show anything, and usefully displaying
that is infeasible. There's always the possibility that I'll want to revert
to the chess clock setup, so aim to leave the possibility of uploading and
viewing failed attempts.

Tempting to keep the client wholly local, letting the user use other means
(http, email, ftp, whatever) to communicate files with the server(s). But
perhaps that would be too annoying.

Alternatively, we have some nice simple cgi script as the server, and the
client uses curl or whatever to talk to it. So we still deal locally with
files, we just automate the process of uploading and downloading the files.

So we have a few basic high-level operations for the client, which should be
launchable independently with appropriate command-line options, but which also
get appropriately twisted together in the default mode:
    edit/create lockfile (producing canonical lockfiles)
    play lock (taking lockfile, producing solution)
    view solution (taking lockfile and solution)

Metagame interactions with the server:
    buying coffers and selecting locks for them
    adjusting distribution of picks between coffers
    choosing coffers to try to crack

Tempting to have these all done purely with cgi+html. But recall just how
horrible html interfaces are.

Instead, I think I like the idea of using a simple textual interface for
these, with displays of your locks mixed in where appropriate, which can
easily be uniform over UIs (recall that in text-mode, a lock takes up only
25 out of 80 columns, and in SDL we can scale arbitrarily). But let's not
worry about details of the UI for now. Suffice to say I see that it could be
done quite neatly - and that having the ability to display the locks is
another good reason to make it all part of the same client.

The server needs to handle:
    sending coffer status - including any solution files
    accepting coffer updates, including new locks

Do we need a database? Or will a directory structure do?
Server:
    locks/LOCKIDENTIFIER/{lockfile,creator,solution,attempts/,picks}
Client:
    no need to use the file system, except for your collection of lockfiles.
    The rest can be kept in memory.

On the server, locks are named after sequential numbers.

But then Jason Rohrer wrote this paeon to sql, which made me reconsider:
http://thecastledoctrine.net/forums/viewtopic.php?pid=2566#p2566

mysql vs sqlite: main difference seems to be that sqlite write-locks the whole
database when any part of it is written to, while mysql is cleverer. sqlite
should be fine for this project, I'm sure.

-----


Remind me: why is this idea of having a metagame a good one and not a silly
	one?
    Answer: because the whole point is to explore the outer reaches of
	complexity within the space of possible puzzles - which given the hard
	restriction on lock size, means exploring the extremes of intricacy.
	For this, we need clear motivation from puzzle designers to design the
	hardest puzzles they can. This is the point of the metagame.

-----

Simplifying the metagame:
One active lock per player.

No penalty for changing the lock you're using.

Players are wholly anonymous (*not* pseudonymous) and before agreeing to try a
lock you're told only the approximate number of picks it protects. This way
there's no real way to abuse multiple accounts, because you can't really
transfer picks between accounts.

So although having a single account should be the default, we can allow the
addicted to create extra.

Actually no, this doesn't work at all - we *have* to avoid the same player
getting two stabs at a lock. Hrm.

Note there *is* anyway a potential for abuse if one person owns a significant
proportion of the accounts... I'll just have to watch out for that.

Given anonymity, no need for usernames - a single secret identifier will
suffice.

Handling revenge with anonymity: just make it an explicit option, a key/button
when watching a replay. But maybe revenge doesn't fit this game anyway.

But maybe the *locks* should get (code)named - at least the successful,
publically displayed ones? That way people can talk about them in sidechannel
discussions, and there can even be publically accessible urls for them.
No need to let the creator choose the name (a recipe for profanity and
SQL-injection...), can use a wordlist.

-----

Need to take seriously this exploit:
    create two accounts (can't hope to prevent this)
    log in with account 1, try a load of locks
    solve them in your own time
    log in with account 2, knowing the solutions to lots of locks.

This isn't preventable, nor is it really detectable. So it's a fatal flaw with
the current design.

So ditch the idea that a user can only try a lock once.

So it seems we're led to: a race to be the first to solve a lock.

So like the current design, but without the time limit, and with no second
prize (lest the same player with multiple accounts take all the prizes).

Obvious worry: solving locks, particularly given undo, is so much quicker than
creating them that it'll be rare that there are any unsolved locks available.

This may not be too bad if the userbase is large and active enough. If I spend
an hour designing a fiendish lock which someone then picks in ten minutes,
that's arguably not a problem if at least five others were also trying to pick
it during those ten minutes. If the solver is the only person who ever sees
it, we have a decided imbalance.

But maybe this is overly simplistic - as long as the design stage is
sufficiently painless and engaging, spending most of your time there shouldn't
be a problem. Still, it's an argument e.g. for keeping the balls, making
greater intricacy easier to achieve. Possibly relaxing size limits would do
this too (on the basis that squeezing an intricate lock into a small space
takes extra ingenuity).

We need to think about incentives. Unoriginality - e.g. just using the same
lock over and over again, or copying someone else's - is naturally discouraged
since such a lock is likely to be solved very quickly. (Note that we don't
have to worry about someone copying a lock design before solving it
themselves, thanks to the self-test.)

We need to set things up to foil another obvious exploit: setting a lock with
one account and then solving it with another. It's ok for this to result in a
flow of points, but it shouldn't be a net gain if the solution is given
quickly. I suppose it won't be possible to foil this strategy completely,
since for a decently hard lock we do want to reward both setter and solver...
but it is at least relatively easy to detect. We could also try anonymisation
techniques (which would have to include adding a random delay, invisible to
the setter, between a lock being set and it becoming available...) but there
will anyway be a systematic advantage to be gained from sock-puppetry.

Cunning solution to all of these problems: information as currency! See below.

------

Information-based economy
=========================

Cryptocurrencies and attempts by backwards-looking business-types aside, an
information-based economy differs crucially from a resource-based one, in that
information can be copied.

This deals with all sock-puppet problems.

How I'm thinking this should work:

When you solve a lock, you take notes. You store your notes behind your lock.
When you crack a lock, you take a copy of everything behind it, adding them to
your own stash. Your "score" is the number of locks for which you have a
solution (whether discovered yourself or copied).

Consequences:
    It is important to set a strong lock, to protect the secrets to which you
    become privy. Doing so adds something to the game "economy" (weak
    metaphor), which dissipates slowly and most likely only after the lock has
    been independently cracked by a fair few players. 

    Problem: optimal play would have you change your lock as soon as it's been
    cracked, since knowledge of how to crack it will spread exponentially, so
    it will soon become useless for protecting new secrets. (Note that for the
    secrets it originally protected, it's too late - they will spread
    exponentially anyway.) This could prove tiresome, and more to the point
    will mean a lock doesn't provide as much entertainment for solvers.
    Solution: don't allow changing a lock or removing what it guards, but do
    allow setting a new active lock behind which newly acquired secrets will
    go.
    Problem with solution: now optimal play is to create a new lock for each
    lock you crack, leading to exponential growth in number of uncracked
    locks.

Thought: notes needn't give full solutions, they could just give hints - e.g.
a snapshot of the gamestate at a random point during the solution. Problem
with that: players could take a deliberately contorted path to the solution,
such that a random snapshot might well be misleading.
Or: similar, but automated - say you need to gather notes resulting from n
independent solutions of a lock, and then you automatically solve the lock.


Revised version:
----------------
Each time you solve a lock, you take "notes". In order to "claim" your
solutions, you must design locks of your own behind which to secure your
notes. Behind each lock goes n notes. Whenever you claim a solution to a lock,
you get to see the n notes it secures. Once you have seen k notes on the same
lock, you automatically solve it (and get to see the notes it secures, which
may result recursively in further automatic solving).

Your "score" is the number of locks you have claimed solutions for - direct or
automatic. Locks you have set yourself count towards this.

Effects of parameters:
    b:=n/k is the branching number: solving a lock lets you solve on average
    b^t of the locks t layers back. We want b>1, so old locks effectively lose
    value over time. 

    k is the "fragmentation index", and controls the relative value of
    designing and solving locks.

    n=3, k=2 seems like a decent first thing to try, twiddling later if it
    seems appropriate.

Subtleties:
    The tougher locks will be more valuable, since solving one will increase
    your relative score for longer. So it's in your interest to put your notes
    on them behind your own most ingenious locks. So the hardest locks should
    keep their value for the longest.

    We shouldn't force players to claim their solutions as soon as possible,
    because that could be got around by working out how to solve locks but
    avoiding actually doing so until you're ready to claim them, and also
    because players who enjoy solving might not bother to design a sound lock.
    But note that there's a strong incentive to claim quickly - the value of a
    solution depreciates exponentially as others solve the lock.

    What I mean by 'value' of a solution should maybe be spelt out: solving a
    new lock gives you (on average) b^t solutions of locks of "age" t, where
    (as discussed above) easier locks age much faster than hard ones. So by
    having score be a simple count of the number of locks you have solutions
    to, we get this natural depreciation.
    
    Note that this means that coming in late as a new player, you can catch up
    with the old hands quickly by solving some of the latest locks (and
    there'll be a nice rich bank of old locks to study if the new ones are too
    hard).

Potential for cheating:
    Devise a tricky lock, and learn how to solve k existing locks.
    Create a whole cupboard of sock puppets. Have each puppet solve those k
    locks and put the notes behind that tricky lock. Then have your main
    account solve each of these many instances of that tricky lock. Result:
    your main account gets a cupboard of points. Arse.

Another problem:
    You could deliberately solve earlier locks before the later ones, to
    maximise the number of notes you get and hence the number of locks you can
    set. This is grinding, and rational. That's a problem.

Recovery:
    What happens if we just explicitly allow that kind of "cheating"? That
    comes down to: allow setting a lock which secures nothing, with you still
    getting a point for it. Answer: spam. Hrm.


Another separate worry:
    What about public accounts? If a whole load of players use the same
    account, it would end up as a spoilerbank. Could try to detect multiple
    simultaneous uses, I guess.

Rethinking motivation:
    Aim is to motivate designing locks which can't be easily solved by the
    current playerbase.

    Simplest way to do that: each player designs a lock, and they're ranked
    according to how long they've remained unsolved. But that gives locks a
    very short life - solved once then valueless. And there wouldn't be much
    interest in trying to solve any lock other than the currently top-rated
    one. So something more subtle is required.

    Designing locks as a means to protect your winnings from solving them is
    a natural and appealing setup. Given sock-puppets, these winnings must be
    of a kind you can only obtain from other humans. Which has to mean
    solutions to puzzles, which you yourself might not be able to solve. We
    want these puzzles to be player-produced, so naturally they should be the
    locks themselves. So the basic design above seems pretty much inevitable.

    The tricky bit is to motivate trying to keep your solutions secret, while
    avoiding potential for abuse. If we simply motivate having solutions
    others don't, we clearly motivate sock-puppetry. We want to motivate
    having solutions *to the locks of other humans* which others don't.

    How about if we drop the idea of a linear scale. A dominates B iff A can
    crack more of B's locks than can B A's. The point being that you're not
    going to care about dominating your sock puppets. Problem: you don't
    really care about others dominating those you dominate, either, so won't
    care about protecting your secrets.

    But consider Ultra. You want to keep your solution to a lock to yourself
    because you don't want the owner of the lock to stop using it. So we need
    a cost associated to setting up a new lock, such that you're motivated to
    keep using an old one until it's been too thoroughly compromised.

New version
-----------

You're allowed at most l active locks at any time. To declare a solution
to a lock, you put your notes on it behind one of your active locks. If
you deactivate one of your locks, then its solution along with all notes
it secured become common knowledge. You have access to all public
knowledge notes and to all notes secured by locks to which you have a
declared or automatic solution, and having access to k notes on a lock
gives you an automatic solution to it. There is no absolute scoring;
rather, A scores a point against B for each of B's locks to which A has a
solution, and each player sees other players ranked according to relative
score. Optionally, you can get fractional points for having notes on a lock.

Player motivation:
    If too many players have solutions to one of your active locks, you'll
    want to ditch it. So if you have a solution to someone's active lock, you
    won't want too many others to get solutions to it. So you want to protect
    your notes. So you will put your most valuable notes behind your hardest /
    least compromised lock. So notes to harder locks will be more valuable.

    Note that it can be rational to put notes behind your more compromised
    locks, in order to spread risk and not make your most valuable lock too
    tempting a target. For this to work, it should be public knowledge what
    notes are secured by each of your locks.

Parameters:
    Increasing l lengthens the active life of a lock. It also makes the
    scoring finer.

    Large k reduces the importance of lock compromisation, and in particular
    also lengthens the active life of a lock.

    l=3 k=3 sounds good.

Potential for abuse:
    Sock puppets:
	You can set locks as a sock-puppet and solve them with your main
	account, but this will affect only the relative score between these
	two accounts.

	You could scare someone into thinking their lock is utterly
	compromised by solving it with many accounts. But this wouldn't be
	rational - if they ditch the lock, its solution and contents become
	public knowledge, so your situation is only worsened.

	You could be a complete arse and create lots of mirror accounts, which
	copy your every action. With enough mirrors, players wouldn't waste
	their time solving every copy of your locks, so whichever account you
	decide is the real one would get an advantage. This could be made more
	difficult with IP checks and checking for identical locks, but can't
	really be prevented. But the only motivation for doing this is to show
	off that you can break the game if you want to, so hopefully a "yes
	yes, well done" followed by manual purging of the accounts would
	suffice.
	
    Shared accounts and other conspiracies:
	We can't prevent these. Discouragement and banning discovered accounts
	should suffice.

Possible complications:
    Could incorporate variable locksize by allowing one large lock in place of
    two normal sized locks, and two small locks in place of one normal.

    Hints: having 0<n<k notes could give you hints on solving the lock. To
    minimise potential for planting false information, this should consist of
    views of the final, solved state of the lock, as obtained by the solver.
    For n=k-1, this should be the whole final state; for smaller n, some part
    of it.

Other remarks:
  Locksize(s) for new locks should be server-configurable (so I can fiddle
  with it as necessary).

  I worry that the design rewards people for playing more as well as for
  playing better. But there's no unabusable way to restrict it - you can
  always log in with sock puppets, obtain some locks, spend however long you
  need cracking them, and then use this information with your main account.


------

Metagame UI
===========

Even with relative scoring, sock-puppetry would be an annoyance to other
players. So showing anything like a total of the relative scores would
be a bad idea - it could motivate a player to sock-puppet just to boost
that total, even if it's only going to be shown to sem.

So relative scores should be shown one-by-one, not tallied.

But for each player, I think it is important to give a single number for
the relative score, something you're directly motivated to increase,
rather than just describing the details of how you relate to that
player. The relative score could be called something like the "advantage
over" the player; recall it's calculated as
[number of seir locks I can solve] - [number of my locks sey can solve].
Forget the idea of fractional score, that would just complicate things.

When you create an account, you choose a three-character codename.

The main metagame display will show your three active locks; below each
lock is listed the locks notes on which are secured by the lock, above
each lock information on who has solved it (or a warning if its solution
is common knowledge), and somewhere on the screen a random selection of
other players who have no particular relation to you. In all cases, the
player concerned will be represented by seir codename along with your
score relative to sem (with both being colour coded for sign and
magnitude of the score, so +3 is bright green etc). Selecting a player
will switch to a version of the main view for sem, showing seir locks
and notes and notednesses, and some text of the form "You hold
a slight/substantial/absolute advantage over ZGZ" or "ZGZ holds
a slight/substantial/absolute advantage over you" or "You are even with
ZGZ", with the adjective being colour coded with the same colours as the
numbers. Then some detail on which of seir locks you've solved and which
of yours sey's solved.

(Selecting one of seir locks will let you try to solve it. Selecting one
of your locks gives you the option of deactivating it and replacing it
with one of the locks in your library; designing locks to go in your
library is done in a separate interface.)

Hopefully this will be enough to encourage players to try to increase
the numbers.

Note in particular that if some of your locks have common-knowledge
solutions (i.e. if three players have solved the lock and then
deactivated the locks which secured their notes on your lock) the
randoms shown to the right will all be red.


Monochrome mockup for the curses version of this in an 80x24 terminal
follows. Graphical version will be similar, but the actual locks will be
shown (the graphics for that being arbitrarily scalable). For
terminals which are a bit larger, the same can be done in the curses
version. The words in the centre are names for the locks, given by and
seen only by yourself (these are actual names of locks in my own
library).

--------------------------------------------------------------------------------
                                      ZGZ
                                  (That's you)
                         |                           |
                         |                           |
      Cracked by:        |       Cracked by:         |        Cracked by:
                         |                           |
    FKU -2    +++ +1     |        Everyone!          |         [no-one]
                         |                           |
                         |                           |
           A             |            B              |            C
        cannon           |         nameless          |         tricksy
                         |                           |
     Holds notes on:     |      Holds notes on:      |       Holds notes on:
                         |                           |
        C:NGS +2         |   A:NGS +2    C:+++ +1    |    B:NGS +2  B:+++ +1
                         |                           |    A:#$% 0   C:ARG +1 
                         |                           |         A:AAA 0
                         |                           |
  Random players:   CNT -1   ^&^ -1   #Z# +1   +3! -1   YHW 0
	[more random players; I can't be arsed to come up with more codenames]

  UNDECLARED solutions:   B:BLL -1  A:#Z# +1

  x: examine player  c: change lock  d: declare solution  e: edit locks  q: quit  
--------------------------------------------------------------------------------

If I then type 'x', it will prompt me for a codename (which doesn't have
to be one displayed on the screen); if I type '#Z#' I'll get the
screen below. (In graphical mode, I could achieve the same effect by
clicking on '#Z#' wherever in the above.)

--------------------------------------------------------------------------------
                                      #Z# +1
                     You hold a slight advantage over #Z#
                         |                           |
                         |                           |
      Cracked by:        |       Cracked by:         |        Cracked by:
                         |                           |
        BLA -1           |  NGS +2   YHW 0   ARG +1  |         Everyone!
                         |           ZGZ -           |
                         |                           |
          A              |             B             |             C
  Cracked but UNDECLARED |   Cracked! (from notes)   |   Cracked! (from notes)
                         |                           |
     Holds notes on:     |      Holds notes on:      |       Holds notes on:
                         |                           |
        A:NGS +2         |     B:NGS +2  A:+++ +1    |    B:YHW 0   C:ZGZ - 
                         |                           |         B:+3! -1
                         |                           |
                         |                           |
     You have cracked 2 of #Z#'s locks; #Z# has cracked 1 of your locks.
	A:ZGZ - 1 note read by #Z#
	B:ZGZ - cracked directly by #Z#


	      x: view player  c: crack lock  m: view ZGZ  q: quit  
--------------------------------------------------------------------------------

Note that the scores shown here are still those relative to me, not to
#Z#. 

In this example, I have read three notes on each of B and C, which is
why I've cracked them; if I had read only 2 notes, it would say "read
2 notes" in place of "Cracked! (from notes)".

Note that players who have cracked a lock from notes appear in the
"Cracked by:" section, undifferentiated from those who cracked it
directly. If there are too many to display, a random selection is
displayed (ending with "and N more").

Note that I'll use colour to pick out the important words where I've
written out blocks of text. I'll probably also use consistent colours
for 'A', 'B', and 'C' when they are indicating locks.

Note also that I've started using the terminology "cracked" rather than
"solved". Obviously this is by analogy with crypto... not sure how
appropriate it is!


Thoughts on automatically gauging difficulty of a lock, so as to allow a "hall
of fame" for the best retired locks: basing it on how long the lock lasts
isn't much good, since if no notes are put behind, it could just survive in
obscurity. Anything involving seeing how many attempt a lock would be
gameable. Similarly anything involving counting notes. Hmm.

Maybe no need for that. Just let players see the retired locks of other
players, and maybe have players decide which of their retired locks they're
most proud of so should be shown first. Hmm. Or maybe just ignore this problem
for now.


------

Architecture
------------

Size estimates:
    metagame stuff:
	for each user
	    codename: C bytes
	    serial numbers for active locks: 3*L bytes
	    *who's solved them (and whether directly): S*(C+1) bytes
	    how many notes on each lock are public knowledge: 3 bytes
	    notes declared: N*(C+L+1) bytes
	    *notes read: R*(C+L) bytes
	C=3.
	L=3 is plenty.

	This information, along with public knowledge, suffices to display the
	info screen for the user.

	Starred entries can theoretically be computed just from knowing all
	the declared notes, but not cheaply.
    lock:
	Show + gzip: ~1K
	based on ascii representation: 30<2^5 possibilities per cell, 175 cells
	    875 bits; 110 bytes
	Data.Binary + gzip: ~450 bytes. Hrm.

    solution: 10<2^4 commands, say average 100 moves: 50 bytes.
	gzipping might work on it.

Let's dream big, and say we want to be able to support 5000 players. With
three locks each, that's about 1.5M of lock data.

Meanwhile, with S=30, N=20, R=40, we get 3+9+90+120+240 =~ 460 bytes per
player for metagame data. So around 2.5M.

We don't want the server to have to push 4M of data to each of 5000 players.

So on-demand is the way to go. Locks and solutions are permanent, so can and
should be cached permanently. User entries can be too - but checked for
updates whenever displayed.


Communication we want to support:
    client->server
	Setting locks
	Declaring solutions
	    these can block on verification
    client<-server
	Updating user info
	Get lock
	Get (partial) solution

We should send only deltas where possible. So user info and PK are versioned
with serial numbers; the server stores the latest version and the previous N
deltas, and if requested an update from a recent enough version sends only the
deltas, else sends the whole latest version.

Sounds like we're hand-rolling, then. Fine by me!

Server consists of a simple CGI script to handle incoming locks and solutions
    verifies account ownership
    fires up intricacy to verify solutions
    updates files appropriately

For getting stuff off the server, we can use straight HTTP for locks (which
    are public knowledge and don't change). For the rest, we can use a CGI
    script, which can send deltas when appropriate and verify passwords and
    privs when requesting solutions.

Updates:
    On solution declaration:
	Add as read note for all who have access to the lock it's stored
	behind; this may result in that user getting three notes (public
	notes included), leading to that user reading extra notes.
    On lock change:
	All notes behind the lock become public knowledge. These locks may
	themselves become public knowledge, or become cracked by those who
	have read sufficient notes on them, which as above results in them
	reading notes.

Security:
    No need for TLS; just encrypt all requests to the server's public key.
    One such request is 'reserve codename', and we send a password with that
    and all future requests requiring authentication.

    Fine to ignore this for now, and just send everything in the clear. Can
    add crypto later.

PoW:
    Would be easy to require a little proof of work when reserving a
    codename... but I don't think it's a good idea.


-----
Caching - implementation details
--------------------------------

Use STM. When the UI wants a userinfo, we
    infoTV <- atomically $ newTVar (Nothing :: Maybe UserInfo)
    forkIO getUserInfo infoTV
then record infoTV in the UI state, and then whenever we want to draw it or
whatever, we get at it like this:
    atomically $ readTVar infoTV >>= maybe retry return
meanwhile, the 'getUserInfo' thread first looks in the cache and writes any
result to infoTV, and then consults the server and updates infoTV (and the
cache) if appropriate.

Neato.

The cache can use the Database module, first cding to a dir named after the
server (hostname[:port]).

Implemented.

But to know what colour to draw a codename, we need to calculate the relative
score, which involves having the userinfos of both players.

So:
    * we should put any cached info in the tvar right from the start, so we
	don't redraw the screen after each file is read from the disk;
    * we really need a versioning mechanism for userinfo, with the server
	only sending over the userinfo when the cache is stale (and preferably
	only sending diffs)
    * we need some nice abstract mechanism for handling stuff calculated from
	tvar-held information
    * arguably we should cache the calculated scores in memory, recalculating
	only if we get new pertinent information from the server
    * to minimise server-hammering, we should have a timeout before which a
	uinfo won't be checked for freshness when it's being used only for
	score calculation.
	(System.Time.TOD secsSinceEpoch _ <- System.Time.getClockTime)


-----
Metagame UI architecture
------------------------

IMMeta.

Single view for everything:
    Server, along with indicator of pending requests, with the option to
    select cache-only mode.

    Lock library, with currently selected lock name shown, with preview in SDL
    mode, and buttons/keys to change, to edit, and to set as current lock - to
    create a new one, first set the name to something not yet in the library.

    Tutorials.

    Currently viewed codename. If not yet authenticated: if server says
    codename is free, option to register it; else, option to authenticate.
    After successful auth/reg, send the auth with all future server requests.

    Userinfo, if appropriate.

    Message/Promt line.

In curses mode, this comes down to adding a couple of lines above the above
mockup, assuming default keybindings:

(S) server: gonzales.homelinux.org:27001  [...]	 (C) cache only     (T) tutorial
(L) lock: plug2              (e) edit   (s) set   (n/p) next/prev  (R) rename
                                      ZGZ
				  (R) register


IMConfirm should be removed, becoming a special cross-IM case of prompting. In
both curses and SDL UIs, this is handled with a prompt line at the bottom.

Lock library: just a directory within the confdir containing lockfiles. Should
be able to enter any path, relative or absolute, with relative paths
interpreted as relative to the default lock dir and with mkdirhier applied as
appropriate. All locks stored with ppshow, so revision control systems etc
won't be useless. Locks shown in order of modification. Tab completion up to
longest unique stem, and then cycling through completions.

Undeclared notes: need to clear out notes when they become undeclarable


Terminology:
    Locks are placed, then solved, then accessed.
    Notes are taken on a lock; declared; secured behind a lock; read.

